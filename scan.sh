#!/bin/bash
ips=''
pis=''
function greth(){
        for x in `/sbin/ifconfig |egrep -v "^ "| awk '{print $1}'`
        do
                iface=${x//:}
                #if [[ $(echo "$iface" | grep -o "^e") ]]; then
                        echo $(ip -4 a s ${x//:} up | grep -Eo '((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])' | grep -m1 '.*')
                #fi
        done
}

function slowscan () {
        if (( $# >= 2 ))
        then
                echo "usage: $FUNCNAME [ip[/mask]]"
                echo "example: $FUNCNAME"
                echo "example: $FUNCNAME 192.168.0.1/24"
        fi
       if (( $# == 0 ))
        then
	    add=$(greth)
	    for x in $add
	    do
		if [[ $x != '127.0.0.1' ]]; then
			scan=$(nmap -T3 -sP --max-hostgroup 50 --min-rate 100000 "$x"/24)
    			echo "=========== $x ==========="
	       		cleanscan $scan
		fi
	    done	   
        else
                nmap -T3 -sP --max-hostgroup 50 --min-rate 100000 "$1"
        fi
}

function cleanscan (){
  regex1='((for\s)(\S+)\.lan\s\()?(((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]))'
  i=0
  IFS=$'\n' #ZEILWEISE LESEN
  for f in $1
  do
    if [[ $f =~ $regex1 ]]
    then
      echo "${BASH_REMATCH[4]} ${BASH_REMATCH[3]}"
      ips[i++]="${BASH_REMATCH[4]} ${BASH_REMATCH[3]}"
    fi 
  done
  IFS=''
}

slowscan
